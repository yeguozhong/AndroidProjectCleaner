package com.darcye.clear.test;

import java.io.FileNotFoundException;

import com.darcye.clear.reader.JavaFileReader;

public class JavaFileReaderTester {
	
	private static final String TEST_FILE = "E:\\workspace\\AProjectClear\\src\\testfile\\BuyActivity.java";
	
	public static void main(String[] args)  {
		testObtainLayouts();
	}
	
	public static void testObtainLayouts(){
		JavaFileReader f = new JavaFileReader(TEST_FILE);
		try {
			f.scan();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
}
