package com.darcye.clear.test;

import com.darcye.clear.work.AttrWorker;
import com.darcye.clear.work.JavaWorker;

public class CleanAttrTester extends BaseTester {
	public static void main(String[] args) {
		JavaWorker javaWorker = new JavaWorker();
		javaWorker.execute();
		AttrWorker attrWorker = new AttrWorker(javaWorker);
		attrWorker.execute();
	}
}
