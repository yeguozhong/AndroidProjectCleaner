package com.darcye.clear.test;

import com.darcye.clear.work.IdWorker;
import com.darcye.clear.work.JavaWorker;
import com.darcye.clear.work.LayoutWorker;
import com.darcye.clear.work.ManifestWorker;
import com.darcye.clear.work.MenuWorker;
import com.darcye.clear.work.StyleWorker;

public class CleanIdTester extends BaseTester {
	public static void main(String[] args) {
		JavaWorker javaWorker = new JavaWorker();
		javaWorker.execute();
		MenuWorker menuWorker = new MenuWorker(javaWorker);
		menuWorker.execute();
		LayoutWorker layoutWorker = new LayoutWorker(javaWorker, menuWorker);
		layoutWorker.execute();
		ManifestWorker manifestWorker = new ManifestWorker();
		manifestWorker.execute();
		StyleWorker styleWorker = new StyleWorker(javaWorker, layoutWorker,manifestWorker);
		styleWorker.execute();
		IdWorker idWorker = new IdWorker(javaWorker, layoutWorker, menuWorker, styleWorker);
		idWorker.execute();
	}
}
