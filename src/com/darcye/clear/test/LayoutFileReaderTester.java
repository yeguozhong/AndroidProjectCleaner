package com.darcye.clear.test;

import java.io.File;
import java.io.FileNotFoundException;

import com.darcye.clear.reader.LayoutFileReader;

public class LayoutFileReaderTester {
	
	private static final String TEST_FILE = "E:\\workspace\\AProjectClear\\src\\testfile\\activity_about.xml";
	
	public static void main(String[] args) {
		testObtainLayouts();
	}
	
	public static void testObtainLayouts(){
		LayoutFileReader r  = new LayoutFileReader(new File(TEST_FILE));
		try {
			r.scan();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
