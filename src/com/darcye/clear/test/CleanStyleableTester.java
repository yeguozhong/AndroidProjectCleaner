package com.darcye.clear.test;

import com.darcye.clear.work.JavaWorker;
import com.darcye.clear.work.StyleableWorker;

public class CleanStyleableTester extends BaseTester {
	public static void main(String[] args) {
		JavaWorker javaWorker = new JavaWorker();
		javaWorker.execute();
		StyleableWorker styleableWorker = new StyleableWorker(javaWorker);
		styleableWorker.execute();
	}
}
