package com.darcye.clear.test;

import com.darcye.clear.work.JavaWorker;
import com.darcye.clear.work.MenuWorker;

public class CleanMenuTester {
	
	public static final String PROJECT_PATH = "E:\\workspace\\AndroidClearExample";
	
	public static void main(String[] args) {
		JavaWorker.setProjectPath(PROJECT_PATH);
		JavaWorker javaWorker = new JavaWorker();
		javaWorker.execute();
		MenuWorker menuWorker = new MenuWorker(javaWorker);
		menuWorker.execute();
	}
}
