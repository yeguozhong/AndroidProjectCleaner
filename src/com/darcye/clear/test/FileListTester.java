package com.darcye.clear.test;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;

import com.darcye.clear.utils.FileList;

public class FileListTester {
	public static void main(String[] args) {
		FileList fl = new FileList("E:\\workspace\\AProjectClear");
		List<File> files = fl.recurseListFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(".xml");
			}
		});
		for (File file : files) {
			System.out.println(file.getPath());
		}
	}
}
