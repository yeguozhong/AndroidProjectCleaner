package com.darcye.clear.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.darcye.clear.reader.StyleFileReader;
import com.darcye.clear.reader.StyleFileReader.OnStyleFileReaderListener;
import com.darcye.clear.reader.entity.StyleEntity;

public class StyleFileReaderTester {
	public static void main(String[] args) {
		StyleFileReader tester = new StyleFileReader(new File("E:\\workspace\\AndroidClearExample\\res\\values\\styles.xml"));
		try {
			tester.setOnStyleFileReaderListener(new OnStyleFileReaderListener() {

				public void onComplete(Map<String, StyleEntity> styleEntityMap) {
					for (Entry<String, StyleEntity> entry : styleEntityMap.entrySet()) {
						System.out.println(entry.getValue());
					}
				}
				
			}).scan();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
