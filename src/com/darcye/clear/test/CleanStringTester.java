package com.darcye.clear.test;

import com.darcye.clear.work.AnimWorker;
import com.darcye.clear.work.ArrayWorker;
import com.darcye.clear.work.DrawableWorker;
import com.darcye.clear.work.JavaWorker;
import com.darcye.clear.work.LayoutWorker;
import com.darcye.clear.work.ManifestWorker;
import com.darcye.clear.work.MenuWorker;
import com.darcye.clear.work.StringWorker;
import com.darcye.clear.work.StyleWorker;

public class CleanStringTester extends BaseTester {
	public static void main(String[] args) {
		JavaWorker javaWorker = new JavaWorker();
		javaWorker.execute();
		ArrayWorker arrayWorker = new ArrayWorker(javaWorker);
		arrayWorker.execute();
		MenuWorker menuWorker = new MenuWorker(javaWorker);
		menuWorker.execute();
		LayoutWorker layoutWorker = new LayoutWorker(javaWorker, menuWorker);
		layoutWorker.execute();
		ManifestWorker manifestWorker = new ManifestWorker();
		manifestWorker.execute();
		StyleWorker styleWorker = new StyleWorker(javaWorker, layoutWorker,manifestWorker);
		styleWorker.execute();
		AnimWorker animWorker = new AnimWorker(javaWorker, layoutWorker, styleWorker);
		animWorker.execute();
		DrawableWorker drawableWorker = new DrawableWorker(javaWorker, layoutWorker, arrayWorker,manifestWorker, styleWorker, menuWorker, animWorker);
		drawableWorker.execute();
		StringWorker stringWorker = new StringWorker(javaWorker, layoutWorker, arrayWorker, styleWorker, manifestWorker, menuWorker, animWorker);
		stringWorker.execute();
	}
}
