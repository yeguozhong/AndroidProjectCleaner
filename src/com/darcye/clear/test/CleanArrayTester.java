package com.darcye.clear.test;

import com.darcye.clear.work.ArrayWorker;
import com.darcye.clear.work.JavaWorker;

public class CleanArrayTester extends BaseTester {
	public static void main(String[] args) {
		JavaWorker javaWorker = new JavaWorker();
		javaWorker.execute();
		ArrayWorker arrayWorker = new ArrayWorker(javaWorker);
		arrayWorker.execute();
	}
}
 