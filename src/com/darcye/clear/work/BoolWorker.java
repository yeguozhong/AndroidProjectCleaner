package com.darcye.clear.work;

import java.io.File;
import java.io.FilenameFilter;

/**
 * 扫描和清理values(*)目录下的所有文件
 * @author Darcy
 * @date 2014-10-10 下午4:53:23
 * @version V1.0
 */
public class BoolWorker extends BaseWorker {

	private final FilenameFilter mBoolFileFliter = new FilenameFilter() {
		public boolean accept(File file, String fileName) {
			if(file.getParentFile() == null || !file.isFile()) return false;
			String pPathName = file.getParentFile().getName();
			return pPathName.startsWith("values");
		}
	};
	
	public BoolWorker(JavaWorker javaWorker, LayoutWorker layoutWorker,
			ArrayWorker arrayWorker, StyleWorker styleWorker,
			ManifestWorker manifestWorker, AnimWorker animWorker,
			DrawableWorker drawableWorker, MenuWorker menuWorker) {
		mUsedBoolIdSet.addAll(javaWorker.getUsedBoolIdSet());
		mUsedBoolIdSet.addAll(layoutWorker.getUsedBoolIdSet());
		mUsedBoolIdSet.addAll(arrayWorker.getUsedBoolIdSet());
		mUsedBoolIdSet.addAll(styleWorker.getUsedBoolIdSet());
		mUsedBoolIdSet.addAll(manifestWorker.getUsedBoolIdSet());
		mUsedBoolIdSet.addAll(animWorker.getUsedBoolIdSet());
		mUsedBoolIdSet.addAll(drawableWorker.getUsedBoolIdSet());
		mUsedBoolIdSet.addAll(menuWorker.getUsedBoolIdSet());
	}

	public void execute() {
		executeForTerminalId(mBoolFileFliter, "bool", mUsedBoolIdSet);
	}

}
