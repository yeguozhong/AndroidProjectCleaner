package com.darcye.clear.work;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.darcye.clear.utils.FileList;

/**
 * 扫描和清理Id <br/>
 * 
 * 目标文件：values(*)目录下的所有文件
 * @author Darcy
 * @date 2014-10-10 下午4:53:23
 * @version V1.0
 */
public class IdWorker extends BaseWorker {

	private final FilenameFilter mIdFileFliter = new FilenameFilter() {
		public boolean accept(File file, String fileName) {
			if(file.getParentFile() == null || !file.isFile()) return false;
			String pPathName = file.getParentFile().getName();
			return pPathName.startsWith("values");
		}
	};
	
	/**
	 * 关联的资源作为构造方法参数
	 * @param javaWorker
	 * @param layoutWorker
	 * @param menuWorker
	 * @param styleWorker
	 */
	public IdWorker(JavaWorker javaWorker,LayoutWorker layoutWorker,MenuWorker menuWorker,StyleWorker styleWorker){
		mUsedIdIdSet.addAll(javaWorker.getUsedIdIdSet());
		mUsedIdIdSet.addAll(layoutWorker.getUsedIdIdSet());
		mUsedIdIdSet.addAll(menuWorker.getUsedIdIdSet());
		mUsedIdIdSet.addAll(styleWorker.getUsedIdIdSet());
	}
	
	public void execute() {
		executeForTerminalId(mIdFileFliter, "id", mUsedIdIdSet);
	}

}
