package com.darcye.clear.work;

import java.io.File;
import java.io.FilenameFilter;

/**
 * 扫描和清理values(*)目录下的所有文件
 * @author Darcy
 * @date 2014-10-11 上午11:49:45
 * @version V1.0
 */
public class StyleableWorker extends BaseWorker {

	private final FilenameFilter mStyleableFileFliter = new FilenameFilter() {
		public boolean accept(File file, String fileName) {
			if(file.getParentFile() == null || !file.isFile()) return false;
			String pPathName = file.getParentFile().getName();
			return pPathName.startsWith("values");
		}
	};
	
	public StyleableWorker(JavaWorker javaWorker){
		mUsedStyleableIdSet.addAll(javaWorker.getUsedStyleableIdSet());
	}
	
	public void execute() {
		executeForTerminalId(mStyleableFileFliter, "declare-styleable", mUsedStyleableIdSet);
	}

}
