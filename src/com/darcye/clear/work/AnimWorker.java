package com.darcye.clear.work;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.darcye.clear.reader.AnimFileReader;
import com.darcye.clear.reader.OnFileReadListener;
import com.darcye.clear.reader.entity.AnimEntity;
import com.darcye.clear.reader.entity.BaseEntity;
import com.darcye.clear.utils.FileList;

public class AnimWorker extends BaseWorker {
 
	private Set<String> mScanAnimIdSet = new LinkedHashSet<String>();
	private Set<String> mScanAnimatorIdSet = new LinkedHashSet<String>();
	private Set<String> mScanTransitionIdSet = new LinkedHashSet<String>();
	
	private Set<String> mScannedAnimIdSet = new HashSet<String>();
	
	private final static String ANIM_QUALIFIER = "anim";
	private final static String ANIMATOR_QUALIFIER = "animator";
	private final static String TRANSITION_QUALIFIER = "transition";
	
	public AnimWorker(JavaWorker javaWorker, LayoutWorker layoutWorker, StyleWorker styleWorker){
		mUsedAnimIdSet.addAll(javaWorker.getUsedAnimIdSet());
		mUsedAnimIdSet.addAll(layoutWorker.getUsedAnimIdSet());
		mUsedAnimIdSet.addAll(styleWorker.getUsedAnimIdSet());
		
		mUsedAnimatorIdSet.addAll(javaWorker.getUsedAnimatorIdSet());
		mUsedAnimatorIdSet.addAll(layoutWorker.getUsedAnimatorIdSet());
		mUsedAnimatorIdSet.addAll(styleWorker.getUsedAnimatorIdSet());
	
		mUsedTransitionIdSet.addAll(javaWorker.getUsedTransitionIdSet());
		mUsedTransitionIdSet.addAll(layoutWorker.getUsedTransitionIdSet());
		mUsedTransitionIdSet.addAll(styleWorker.getUsedTransitionIdSet());
		
		mScanAnimIdSet.addAll(mUsedAnimIdSet);
		mScanAnimatorIdSet.addAll(mScanAnimatorIdSet);
		mScanTransitionIdSet.addAll(mUsedTransitionIdSet);
	}
	
	public Set<String> getUsedDrawableIdSet() {
		return mUsedDrawableIdSet;
	}

	public void execute() {
		for (String animId : mScanAnimIdSet) {
			recurseScanAnim(animId,ANIM_QUALIFIER);
		}
		for (String animatorId : mScanAnimatorIdSet) {
			recurseScanAnim(animatorId,ANIMATOR_QUALIFIER);
		}
		for (String transitionId : mScanTransitionIdSet) {
			recurseScanAnim(transitionId,TRANSITION_QUALIFIER);
		}
		
		deleteAnimFile(ANIM_QUALIFIER);
		deleteAnimFile(ANIMATOR_QUALIFIER);
		deleteAnimFile(TRANSITION_QUALIFIER);
	}

	private void recurseScanAnim(String resId,String qualifier){
		String resFile = getResourcePath()+"/"+qualifier+"/"+getResFileName(resId);
		if(mScannedAnimIdSet.contains(resFile))return;
		mScannedAnimIdSet.add(resFile);
		AnimFileReader reader = new AnimFileReader(resFile);
		try {
			reader.setOnFileReadListener(new OnFileReadListener() {
				public void onReaded(BaseEntity entity) {
					AnimEntity animEntity = (AnimEntity)entity;
					mUsedDrawableIdSet.addAll(animEntity.getDrawableIdSet());
					mUsedStringIdSet.addAll(animEntity.getStringIdSet());
					mUsedIntegerIdSet.addAll(animEntity.getIntegerIdSet());
					mUsedBoolIdSet.addAll(animEntity.getBoolIdSet());
					Set<String> animIdSet = animEntity.getAnimIdSet();
					if(animIdSet!=null && !animIdSet.isEmpty()){
						mUsedAnimIdSet.addAll(animIdSet);
						for (String animId : animIdSet) {
							recurseScanAnim(animId,ANIM_QUALIFIER);
						}
					}
					Set<String> animatorIdSet = animEntity.getAnimatorIdSet();
					if(animatorIdSet!=null && !animatorIdSet.isEmpty()){
						mUsedAnimatorIdSet.addAll(animatorIdSet);
						for (String animatorId : animatorIdSet) {
							recurseScanAnim(animatorId,ANIMATOR_QUALIFIER);
						}
					}
					Set<String> transitionIdSet = animEntity.getTransitionIdSet();
					if(transitionIdSet!=null && !transitionIdSet.isEmpty()){
						mUsedTransitionIdSet.addAll(transitionIdSet);
						for (String transitionId : transitionIdSet) {
							recurseScanAnim(transitionId,TRANSITION_QUALIFIER);
						}
					}
				}
			}).scan();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private void deleteAnimFile(String qualifier){
		String rootPath = getResourcePath() + "/" +qualifier;
		FileList fl = new FileList(rootPath);
		List<File> animFiles = fl.recurseListFiles(new FilenameFilter() {
			public boolean accept(File file, String name) {
				return name.endsWith(".xml");
			}
		});
		String animFileName;
		for (File animFile : animFiles) {
			animFileName = animFile.getName();
			animFileName = animFileName.substring(0, animFileName.lastIndexOf("."));
			if(qualifier == ANIM_QUALIFIER&&!mUsedAnimIdSet.contains(animFileName)){
				animFile.delete();
			}else if(qualifier == ANIMATOR_QUALIFIER&&!mUsedAnimatorIdSet.contains(animFileName)){
				animFile.delete();
			}else if(qualifier == TRANSITION_QUALIFIER&&!mUsedTransitionIdSet.contains(animFileName)){
				animFile.delete();
			}
		}
	}
}
