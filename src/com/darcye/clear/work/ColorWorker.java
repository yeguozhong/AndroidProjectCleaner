package com.darcye.clear.work;

import java.io.File;
import java.io.FilenameFilter;

/**
 * <p>清理多余的color资源, 这里资源的放置必须符合</p>
 * <ul>
 * <li>1: 在values(*)/(*)color(*).xml文件中</li>
 * <li>1: 在res/color文件夹下</li>
 * </ul>
 * @author Darcy
 * @date 2014-10-9 下午5:49:08
 * @version V1.0
 */
public class ColorWorker extends BaseWorker {

	private final FilenameFilter mColorFileFliter = new FilenameFilter() {
		public boolean accept(File file, String fileName) {
			if(file.getParentFile() == null || !file.isFile()) return false;
			String pPathName = file.getParentFile().getName();
			return (pPathName.startsWith("values")&&fileName.contains("color"))||(pPathName.startsWith("color"));
		}
	};
	
	public ColorWorker(JavaWorker javaWorker,LayoutWorker layoutWorker,ArrayWorker arrayWorker,StyleWorker styleWorker,
								AnimWorker animWorker,DrawableWorker drawableWorker){
		mUsedColorIdSet.addAll(javaWorker.getUsedColorIdSet());
		mUsedColorIdSet.addAll(layoutWorker.getUsedColorIdSet());
		mUsedColorIdSet.addAll(arrayWorker.getUsedColorIdSet());
		mUsedColorIdSet.addAll(styleWorker.getUsedColorIdSet());
		mUsedColorIdSet.addAll(animWorker.getUsedColorIdSet());
		mUsedColorIdSet.addAll(drawableWorker.getUsedColorIdSet());
	}
	
	public void execute() {
		executeForTerminalId(mColorFileFliter,"color" ,mUsedColorIdSet);
	}

}
