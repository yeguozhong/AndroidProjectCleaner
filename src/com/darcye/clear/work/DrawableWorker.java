package com.darcye.clear.work;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.darcye.clear.reader.DrawableFileReader;
import com.darcye.clear.reader.OnFileReadListener;
import com.darcye.clear.reader.entity.BaseEntity;
import com.darcye.clear.reader.entity.DrawableEntity;

public class DrawableWorker extends BaseWorker {
	
	private Set<String> mScanDrawableIdSet = new LinkedHashSet<String>();
	
	private Set<String> mScannedDrawableIdSet = new HashSet<String>();
	
	private List<File> mAllDrawableFiles;
	
	public DrawableWorker(JavaWorker javaWorker,LayoutWorker layoutWorker,ArrayWorker arrayWorker,
			ManifestWorker manifestWorker,StyleWorker styleWorker,MenuWorker menuWorker, AnimWorker animWorker){
		mUsedDrawableIdSet.addAll(javaWorker.getUsedDrawableIdSet());
		mUsedDrawableIdSet.addAll(layoutWorker.getUsedDrawableIdSet());
		mUsedDrawableIdSet.addAll(arrayWorker.getUsedDrawableIdSet());
		mUsedDrawableIdSet.addAll(manifestWorker.getUsedDrawableIdSet());
		mUsedDrawableIdSet.addAll(styleWorker.getUsedDrawableIdSet());
		mUsedDrawableIdSet.addAll(menuWorker.getUsedDrawableIdSet());
		mUsedDrawableIdSet.addAll(animWorker.getUsedDrawableIdSet());
		mScanDrawableIdSet.addAll(mUsedDrawableIdSet);
	}
	
	public void execute() {
		for (String drawableId : mScanDrawableIdSet) {
			recurseDrawableFile(drawableId);
		}
		deleteNoUsedDrawables();
	}
	
	private void recurseDrawableFile(String drawableId){
		if(mScannedDrawableIdSet.contains(drawableId))return;
		mScannedDrawableIdSet.add(drawableId);
		String[] drawablePaths = getDiffDrawablePath();
		File drawableFile;
		DrawableFileReader reader;
		for (String drawPath : drawablePaths) {
			drawableFile = new File(drawPath+"/"+getResFileName(drawableId));
			if(drawableFile.exists()){
				reader = new DrawableFileReader(drawableFile);
				try {
					reader.setOnFileReadListener(new OnFileReadListener() {
						public void onReaded(BaseEntity entity) {
							DrawableEntity drawEntity = (DrawableEntity)entity;
							Set<String> drawableIdSet = drawEntity.getDrawableIdSet();
							mUsedDrawableIdSet.addAll(drawableIdSet);
							mUsedColorIdSet.addAll(drawEntity.getColorIdSet());
							mUsedIntegerIdSet.addAll(drawEntity.getIntegerIdSet());
							mUsedBoolIdSet.addAll(drawEntity.getBoolIdSet());
							mUsedDimenIdSet.addAll(drawEntity.getDimenIdSet());
							for (String drawableId : drawableIdSet) {
								recurseDrawableFile(drawableId);
							}
						}
					}).scan();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void deleteNoUsedDrawables(){
		List<File> allDrawableFiles = getAllDrawableFiles();
		String fileName;
		String idName;
		for (File drawableFile : allDrawableFiles) {
			fileName = drawableFile.getName();
			idName = fileName.substring(0, fileName.indexOf("."));
			if(!mUsedDrawableIdSet.contains(idName)){
				drawableFile.delete();
			}
		}
	}
	
	/**
	 * 获取所有Drawable文件(包括图片)
	 * @return
	 */
	private List<File> getAllDrawableFiles(){
		if(mAllDrawableFiles == null){
			mAllDrawableFiles = new ArrayList<File>();
			String[] drawablePaths = getDiffDrawablePath();
			File path;
			for (String drawPath : drawablePaths) {
				path = new File(drawPath);
				mAllDrawableFiles.addAll(Arrays.asList(path.listFiles()));
			}
		}
		return mAllDrawableFiles;
	}
}
