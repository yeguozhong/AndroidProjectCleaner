package com.darcye.clear.work;

import java.io.File;
import java.io.FilenameFilter;

/**
 * 扫描和清理values(*)目录下的所有文件
 * 
 * @author Darcy
 * @date 2014-10-10 下午4:53:23
 * @version V1.0
 */
public class DimenWorker extends BaseWorker {

	private final FilenameFilter mDimenFileFliter = new FilenameFilter() {
		public boolean accept(File file, String fileName) {
			if (file.getParentFile() == null || !file.isFile())
				return false;
			String pPathName = file.getParentFile().getName();
			return pPathName.startsWith("values");
		}
	};

	public DimenWorker(JavaWorker javaWorker, LayoutWorker layoutWorker,
			ArrayWorker arrayWorker, StyleWorker styleWorker,
			DrawableWorker drawableWorker) {
		mUsedDimenIdSet.addAll(javaWorker.getUsedDimenIdSet());
		mUsedDimenIdSet.addAll(layoutWorker.getUsedDimenIdSet());
		mUsedDimenIdSet.addAll(arrayWorker.getUsedDimenIdSet());
		mUsedDimenIdSet.addAll(styleWorker.getUsedDimenIdSet());
		mUsedDimenIdSet.addAll(drawableWorker.getUsedDimenIdSet());
	}

	public void execute() {
		executeForTerminalId(mDimenFileFliter, "dimen", mUsedDimenIdSet);
	}

}
