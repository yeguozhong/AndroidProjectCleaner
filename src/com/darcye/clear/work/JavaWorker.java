package com.darcye.clear.work;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.darcye.clear.reader.JavaFileReader;
import com.darcye.clear.reader.OnFileReadListener;
import com.darcye.clear.reader.entity.BaseEntity;
import com.darcye.clear.reader.entity.JavaEntity;

/**
 * 解析所有Java文件中包含的资源Id
 * @author Darcy
 * @date 2014-9-30 下午3:02:34
 * @version V1.0
 */
public class JavaWorker extends BaseWorker{
	
	public void execute() {
		List<File> javaFiles = getAllJavaFiles();
		for (File jFile : javaFiles) {
			JavaFileReader jFileReader = new JavaFileReader(jFile);
			try {
				jFileReader.setOnFileReadListener(new OnFileReadListener() {
					public void onReaded(BaseEntity entity) {
						JavaEntity javaEntity = (JavaEntity)entity;
						mUsedArrayIdSet.addAll(javaEntity.getArrayIdSet());
						mUsedMenuIdSet.addAll(javaEntity.getMenuIdSet());
						mUsedLayoutIdSet.addAll(javaEntity.getLayoutIdSet());
						mUsedStyleIdSet.addAll(javaEntity.getStyleIdSet());
						mUsedAnimIdSet.addAll(javaEntity.getAnimIdSet());
						mUsedAnimatorIdSet.addAll(javaEntity.getAnimatorIdSet());
						mUsedTransitionIdSet.addAll(javaEntity.getTransitionIdSet());
						mUsedDrawableIdSet.addAll(javaEntity.getDrawableIdSet());
						mUsedColorIdSet.addAll(javaEntity.getColorIdSet());
						mUsedStringIdSet.addAll(javaEntity.getStringIdSet());
						mUsedIntegerIdSet.addAll(javaEntity.getIntegerIdSet());
						mUsedBoolIdSet.addAll(javaEntity.getBoolIdSet());
						mUsedDimenIdSet.addAll(javaEntity.getDimenIdSet());
						mUsedAttrIdSet.addAll(javaEntity.getAttrIdSet());
						mUsedStyleableIdSet.addAll(javaEntity.getStyleableIdSet());
						mUsedIdIdSet.addAll(javaEntity.getIdIdSet());
					}
				}).scan();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
