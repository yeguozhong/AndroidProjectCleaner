package com.darcye.clear.work;

import java.io.File;
import java.io.FilenameFilter;
/**
 * 扫描和清理values(*)目录下的所有文件
 * @author Darcy
 * @date 2014-10-10 下午4:53:23
 * @version V1.0
 */
public class IntegerWorker extends BaseWorker {

	private final FilenameFilter mIntegerFileFliter = new FilenameFilter() {
		public boolean accept(File file, String fileName) {
			if(file.getParentFile() == null || !file.isFile()) return false;
			String pPathName = file.getParentFile().getName();
			return pPathName.startsWith("values");
		}
	};
	
	public IntegerWorker(JavaWorker javaWorker,LayoutWorker layoutWorker,ArrayWorker arrayWorker,StyleWorker styleWorker,
			ManifestWorker manifestWorker,AnimWorker animWorker,DrawableWorker drawableWorker,
			MenuWorker menuWorker){
		mUsedIntegerIdSet.addAll(javaWorker.getUsedIntegerIdSet());
		mUsedIntegerIdSet.addAll(layoutWorker.getUsedIntegerIdSet());
		mUsedIntegerIdSet.addAll(javaWorker.getUsedIntegerIdSet());
		mUsedIntegerIdSet.addAll(arrayWorker.getUsedIntegerIdSet());
		mUsedIntegerIdSet.addAll(styleWorker.getUsedIntegerIdSet());
		mUsedIntegerIdSet.addAll(manifestWorker.getUsedIntegerIdSet());
		mUsedIntegerIdSet.addAll(animWorker.getUsedIntegerIdSet());
		mUsedIntegerIdSet.addAll(drawableWorker.getUsedIntegerIdSet());
		mUsedIntegerIdSet.addAll(menuWorker.getUsedIntegerIdSet());
	}
	
	public void execute() {
		executeForTerminalId(mIntegerFileFliter, "integer", mUsedIntegerIdSet);
	}

}
