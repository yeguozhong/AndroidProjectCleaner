package com.darcye.clear.work;

import java.io.FileNotFoundException;

import com.darcye.clear.reader.ManifestFileReader;
import com.darcye.clear.reader.OnFileReadListener;
import com.darcye.clear.reader.entity.BaseEntity;
import com.darcye.clear.reader.entity.ManifestEntity;

/**
 * <p>读取Manifest文件内的相关资源.</p>
 * <p>目前已发现可能出现的资源类型有<strong>style/drawable/string</strong></p>
 * @author Darcy
 * @date 2014-10-9 下午5:46:27
 * @version V1.0
 */
public class ManifestWorker extends BaseWorker {

	public void execute() {
		ManifestFileReader reader = new ManifestFileReader(getManifestPath());
		try {
			reader.setOnFileReadListener(new OnFileReadListener() {
				public void onReaded(BaseEntity entity) {
					ManifestEntity maniEntity = (ManifestEntity)entity;
					mUsedStyleIdSet.addAll(maniEntity.getStyleIdSet());
					mUsedDrawableIdSet.addAll(maniEntity.getDrawableIdSet());
					mUsedStringIdSet.addAll(maniEntity.getStringIdSet());
					mUsedIntegerIdSet.addAll(maniEntity.getIntegerIdSet());
					mUsedBoolIdSet.addAll(maniEntity.getBoolIdSet());
				}
			}).scan();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
