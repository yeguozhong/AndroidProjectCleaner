package com.darcye.clear.work;

import java.io.File;
import java.io.FilenameFilter;

/**
 * 扫描和清理values(*)目录下的所有文件
 * @author Darcy
 * @date 2014-10-10 下午3:37:07
 * @version V1.0
 */
public class StringWorker extends BaseWorker {

	private final FilenameFilter mStringFileFliter = new FilenameFilter() {
		public boolean accept(File file, String fileName) {
			if(file.getParentFile() == null || !file.isFile()) return false;
			String pPathName = file.getParentFile().getName();
			return pPathName.startsWith("values");
		}
	};
	
	public StringWorker(JavaWorker javaWorker, LayoutWorker layoutWorker,
			ArrayWorker arrayWorker, StyleWorker styleWorker,ManifestWorker manifestWorker,
			MenuWorker menuWorker, AnimWorker animWorker) {
		mUsedStringIdSet.addAll(javaWorker.getUsedStringIdSet());
		mUsedStringIdSet.addAll(layoutWorker.getUsedStringIdSet());
		mUsedStringIdSet.addAll(arrayWorker.getUsedStringIdSet());
		mUsedStringIdSet.addAll(styleWorker.getUsedStringIdSet());
		mUsedStringIdSet.addAll(manifestWorker.getUsedStringIdSet());
		mUsedStringIdSet.addAll(menuWorker.getUsedStringIdSet());
		mUsedStringIdSet.addAll(animWorker.getUsedStringIdSet());
	}

	public void execute() {
		executeForTerminalId(mStringFileFliter,"string", mUsedStringIdSet);
	}

}
