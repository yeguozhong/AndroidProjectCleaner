package com.darcye.clear.work;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.darcye.clear.reader.LayoutFileReader;
import com.darcye.clear.reader.OnFileReadListener;
import com.darcye.clear.reader.entity.BaseEntity;
import com.darcye.clear.reader.entity.LayoutEntity;
import com.darcye.clear.utils.FileList;

/**
 * 清理layout文件
 * @author Darcy
 * @date 2014-9-26 下午6:14:51
 * @version V1.0
 */
public class LayoutWorker extends BaseWorker{
	
	private Set<String> mScanLayoutIdSet = new LinkedHashSet<String>();
	
	private Set<File> mDeleteLayoutFiles = new LinkedHashSet<File>();

	private Set<String> mScannedLayoutIdSet = new HashSet<String>();
	
	public LayoutWorker(JavaWorker javaWorker,MenuWorker menuWorker){
		mUsedLayoutIdSet.addAll(javaWorker.getUsedLayoutIdSet());
		mUsedLayoutIdSet.addAll(menuWorker.getUsedLayoutIdSet());
		mScanLayoutIdSet.addAll(mUsedLayoutIdSet);
	}
	
	public void execute() {
		deepScanLayouts();
		markDeleteLayoutFiles();
	}
	
	/**
	 * 深度扫描layout资源
	 */
	private void deepScanLayouts(){
		for (String layoutId : mScanLayoutIdSet) {
			recurseScanLayout(layoutId);
		}
	}
	
	private void recurseScanLayout(String layoutId){
		if(mScannedLayoutIdSet.contains(layoutId))return;
		mScannedLayoutIdSet.add(layoutId);
		List<File> diffLayoutFiles = getDiffLayoutFiles(layoutId);
		for (File fFile : diffLayoutFiles) {
			LayoutFileReader layoutReader = new LayoutFileReader(fFile);
			try {
				layoutReader.setOnFileReadListener(new OnFileReadListener() {
					public void onReaded(BaseEntity entity) {
						LayoutEntity layoutEntity = (LayoutEntity)entity;
						mUsedLayoutIdSet.add(layoutEntity.getLayoutFile());
						mUsedStyleIdSet.addAll(layoutEntity.getStyleIdSet());
						mUsedAnimIdSet.addAll(layoutEntity.getAnimIdSet());
						mUsedAnimatorIdSet.addAll(layoutEntity.getAnimatorIdSet());
						mUsedTransitionIdSet.addAll(layoutEntity.getTransitionIdSet());
						mUsedDrawableIdSet.addAll(layoutEntity.getDrawableIdSet());
						mUsedColorIdSet.addAll(layoutEntity.getColorIdSet());
						mUsedStringIdSet.addAll(layoutEntity.getStringIdSet());
						mUsedIntegerIdSet.addAll(layoutEntity.getIntegerIdSet());
						mUsedBoolIdSet.addAll(layoutEntity.getBoolIdSet());
						mUsedDimenIdSet.addAll(layoutEntity.getDimenIdSet());
						mUsedIdIdSet.addAll(layoutEntity.getIdIdSet());
						for (String layoutId : layoutEntity.getLayoutIdSet()) {
							recurseScanLayout(layoutId);
						}
					}
				}).scan();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 标识可以删除的layout资源
	 */
	private void markDeleteLayoutFiles(){
		String[] diffLayoutPaths = getDiffLayoutPath();
		FileList fileList;
		List<File> layoutFiles;
		for (String layoutPath : diffLayoutPaths) {
			 fileList = new FileList(layoutPath);
			 layoutFiles =  fileList.recurseListFiles();
			 for(File layoutFile: layoutFiles){
				 if(isDelelteLayoutFile(layoutFile))
					 mDeleteLayoutFiles.add(layoutFile);
			 }
		}
	}
	
	private boolean isDelelteLayoutFile(File layoutFile){
		String fName = layoutFile.getName();
		String layoutId = fName.substring(0, fName.indexOf("."));
		return !mUsedLayoutIdSet.contains(layoutId);
	}
}
