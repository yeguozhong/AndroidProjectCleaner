package com.darcye.clear.work;

import java.io.File;
import java.io.FilenameFilter;

/**
 * 扫描和清理values(*)目录下的所有文件(只针对第二层次的attr标签)
 * @author Darcy
 * @date 2014-10-11 上午11:49:45
 * @version V1.0
 */
public class AttrWorker extends BaseWorker {

	private final FilenameFilter mAttrrFileFliter = new FilenameFilter() {
		public boolean accept(File file, String fileName) {
			if(file.getParentFile() == null || !file.isFile()) return false;
			String pPathName = file.getParentFile().getName();
			return pPathName.startsWith("values");
		}
	};
	
	public AttrWorker(JavaWorker javaWorker){
		mUsedAttrIdSet.addAll(javaWorker.getUsedAttrIdSet());
	}
	
	public void execute() {
		executeForTerminalId(mAttrrFileFliter, "attr", mUsedAttrIdSet);
	}

}
