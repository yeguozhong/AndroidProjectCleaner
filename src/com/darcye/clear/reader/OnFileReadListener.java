package com.darcye.clear.reader;

import com.darcye.clear.reader.entity.BaseEntity;

public interface OnFileReadListener {
	void onReaded(BaseEntity entity);
}
