package com.darcye.clear.reader;

import java.io.File;
import java.util.regex.Pattern;

import com.darcye.clear.reader.entity.LayoutEntity;
import com.darcye.clear.utils.ResRegex;

public class LayoutFileReader extends BaseFileReader {

	/**
	 * 匹配布局文件的正则
	 */
	private static final Pattern resPattern = Pattern.compile(ResRegex.LAYOUT_RES_ID_REGEX);
	
	private LayoutEntity mLayoutEntity = new LayoutEntity();

	//压缩成单行存储
	private StringBuilder mXmlContent = new StringBuilder(); 

	public LayoutFileReader(String fileName){
		this(new File(fileName));
	}
	
	public LayoutFileReader(File file) {
		super(file);
	}

	@Override
	protected void onLineRead(String lineText) {
		mXmlContent.append(lineText);
	}

	@Override
	protected void onFinished() {
		obtainResIds();
		setEntity(mLayoutEntity);
	}

	/**
	 * 获取资源Id
	 */
	void obtainResIds(){
		obtainResIdOnRegex(resPattern, mXmlContent.toString(), new OnObtainResId() {
			public void onObtain(ResType type, String resIdName) {
				switch (type) {
				case LAYOUT:
					mLayoutEntity.addLayoutId(resIdName);
					break;
				case STYLE:
					mLayoutEntity.addStyleId(resIdName);
					break;
				case DRAWABLE:
					mLayoutEntity.addDrawableId(resIdName);
					break;
				case ANIM:
					mLayoutEntity.addAnimId(resIdName);
					break;
				case ANIMATOR:
					mLayoutEntity.addAnimatorId(resIdName);
					break;
				case TRANSITION:
					mLayoutEntity.addTransitionId(resIdName);
					break;
				case COLOR:
					mLayoutEntity.addColorId(resIdName);
					break;
				case STRING:
					mLayoutEntity.addStringId(resIdName);
					break;
				case INTEGER:
					mLayoutEntity.addIntegerId(resIdName);
					break;
				case BOOL:
					mLayoutEntity.addBoolId(resIdName);
					break;
				case DIMEN:
					mLayoutEntity.addDimenId(resIdName);
					break;
				case ID:
					mLayoutEntity.addIdId(resIdName);
					break;
				default:
					break;
				}
			}
		});
	}
}
