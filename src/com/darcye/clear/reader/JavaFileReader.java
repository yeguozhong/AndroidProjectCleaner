package com.darcye.clear.reader;

import java.io.File;
import java.util.regex.Pattern;

import com.darcye.clear.reader.entity.JavaEntity;
import com.darcye.clear.utils.ResRegex;

/**
 * 扫描Java文件
 * 
 * @author Darcy
 * @date 2014-9-26 下午3:22:24
 * @version V1.0
 */
public class JavaFileReader extends BaseFileReader {
	
	/**
	 * 匹配布局文件的正则
	 */
	private static final Pattern resPattern = Pattern.compile(ResRegex.JAVA_RES_ID_REGEX);
	
	private JavaEntity mJavaEntity = new JavaEntity();
	
	private StringBuilder mWholeStatement = new StringBuilder();
	
	public JavaFileReader(String fileName){
		this(new File(fileName));
	}
	
	public JavaFileReader(File file) {
		super(file);
	}

	@Override
	protected void onLineRead(String lineText) {
		if( isComment(lineText) || isAnnotation(lineText))
			return;
		
		if(!isStatementFinished(lineText)){
			mWholeStatement.append(lineText);
			return;
		}else{
			if(mWholeStatement.length() != 0){
				mWholeStatement.delete(0, mWholeStatement.length());
			}
			mWholeStatement.append(lineText);
		}
		
		obtainResIds();
	}

	/**
	 * 获取资源Id
	 */
	void obtainResIds(){
		obtainResIdOnRegex(resPattern, mWholeStatement.toString(), new OnObtainResId() {
			public void onObtain(ResType type, String resIdName) {
				switch (type) {
				case LAYOUT:
					mJavaEntity.addLayoutId(resIdName);
					break;
				case STYLE:
					mJavaEntity.addStyleId(parseStyleId(resIdName));
					break;
				case MENU:
					mJavaEntity.addMenuId(resIdName);
					break;
				case DRAWABLE:
					mJavaEntity.addDrawableId(resIdName);
					break;
				case ANIM:
					mJavaEntity.addAnimId(resIdName);
					break;
				case ANIMATOR:
					mJavaEntity.addAnimatorId(resIdName);
					break;
				case TRANSITION:
					mJavaEntity.addTransitionId(resIdName);
					break;
				case COLOR:
					mJavaEntity.addColorId(resIdName);
					break;
				case ARRAY:
					mJavaEntity.addArrayId(resIdName);
					break;
				case STRING:
					mJavaEntity.addStringId(resIdName);
					break;
				case INTEGER:
					mJavaEntity.addIntegerId(resIdName);
					break;
				case BOOL:
					mJavaEntity.addBoolId(resIdName);
					break;
				case DIMEN:
					mJavaEntity.addDimenId(resIdName);
					break;
				case ATTR:
					mJavaEntity.addAttrId(resIdName);
					break;
				case STYLEABLE:
					mJavaEntity.addStyleableId(resIdName);
					break;
				case ID:
					mJavaEntity.addIdId(resIdName);
					break;
				default:
					break;
				}
			}
		});
	}
	
	/**
	 * <p>对在Java代码中出现的Style Id 进行转换，把下划线转化成. 。</p>
	 * <p><em><strong>注意: </strong>这里假设Style的所有自定义命名规则为不能包括下划线。</em></p>
	 * @param resIdName
	 * @return
	 */
	private String parseStyleId(String resIdName){
		return resIdName.replaceAll("_", ".");
	}
	
	@Override
	protected void onFinished() {
		setEntity(mJavaEntity);
	}
	
	/**
	 * 是否是注释行
	 * @param lineText
	 * @return
	 */
	private boolean isComment(String lineText){
		return lineText.startsWith("/*") || lineText.startsWith("*") || lineText.startsWith("//");
	}
	
	/**
	 * 是否是注解
	 * @param lineText
	 * @return
	 */
	private boolean isAnnotation(String lineText){
		return lineText.startsWith("@");
	}
	
	/**
	 * 语句是否结束
	 * @param lineText
	 * @return
	 */
	private boolean isStatementFinished(String lineText){
		return lineText.endsWith(";");
	}
	
	public void onReadLayout(String layout){}
}
