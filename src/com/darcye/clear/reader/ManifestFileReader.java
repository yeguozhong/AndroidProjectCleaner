package com.darcye.clear.reader;

import java.io.File;
import java.util.regex.Pattern;

import com.darcye.clear.reader.entity.ManifestEntity;
import com.darcye.clear.utils.ResRegex;

public class ManifestFileReader extends BaseFileReader {
	
	/**
	 * 匹配布局文件的正则
	 */
	private static final Pattern resPattern = Pattern.compile(ResRegex.MANIFEST_RES_ID_REGEX);
	
	private ManifestEntity mManifestEntity = new ManifestEntity();
	
	//压缩成单行存储
	private StringBuilder mXmlContent = new StringBuilder(); 

	public ManifestFileReader(String fileName){
		this(new File(fileName));
	}
	
	public ManifestFileReader(File file) {
		super(file);
	}
	
	@Override
	protected void onLineRead(String lineText) {
		mXmlContent.append(lineText);
	}

	@Override
	protected void onFinished() {
		obtainResIds();
		setEntity(mManifestEntity);
	}

	/**
	 * 获取资源Id
	 */
	void obtainResIds(){
		obtainResIdOnRegex(resPattern, mXmlContent.toString(), new OnObtainResId() {
			public void onObtain(ResType type, String resIdName) {
				switch (type) {
				case STYLE:
					mManifestEntity.addStyleId(resIdName);
					break;
				case DRAWABLE:
					mManifestEntity.addDrawableId(resIdName);
					break;
				case STRING:
					mManifestEntity.addStringId(resIdName);
					break;
				case INTEGER:
					mManifestEntity.addIntegerId(resIdName);
					break;
				case BOOL:
					mManifestEntity.addBoolId(resIdName);
					break;
				default:
					break;
				}
			}
		});
	}

}
