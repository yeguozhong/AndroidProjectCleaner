package com.darcye.clear.reader;

import java.io.File;
import java.util.regex.Pattern;

import com.darcye.clear.reader.entity.MenuEntity;
import com.darcye.clear.utils.ResRegex;

public class MenuFileReader extends BaseFileReader {
	
	/**
	 * 匹配布局文件的正则
	 */
	private static final Pattern resPattern = Pattern.compile(ResRegex.MENU_RES_ID_REGEX);
	
	private MenuEntity mMenuEntity = new MenuEntity();
	
	//压缩成单行存储
	private StringBuilder mXmlContent = new StringBuilder(); 

	public MenuFileReader(File file) {
		super(file);
	}

	@Override
	protected void onLineRead(String lineText) {
		mXmlContent.append(lineText);
	}
	
	@Override
	protected void onFinished() {
		obtainResIds();
		setEntity(mMenuEntity);
	}

	/**
	 * 获取资源Id
	 */
	void obtainResIds(){
		obtainResIdOnRegex(resPattern, mXmlContent.toString(), new OnObtainResId() {
			public void onObtain(ResType type, String resIdName) {
				switch (type) {
				case LAYOUT:
					mMenuEntity.addLayoutId(resIdName);
				case DRAWABLE:
					mMenuEntity.addDrawableId(resIdName);
					break;
				case STRING:
					mMenuEntity.addStringId(resIdName);
					break;
				case INTEGER:
					mMenuEntity.addIntegerId(resIdName);
					break;
				case BOOL:
					mMenuEntity.addBoolId(resIdName);
					break;
				case DIMEN:
					mMenuEntity.addDimenId(resIdName);
					break;
				case ID:
					mMenuEntity.addIdId(resIdName);
					break;
				default:
					break;
				}
			}
		});
	}
}
