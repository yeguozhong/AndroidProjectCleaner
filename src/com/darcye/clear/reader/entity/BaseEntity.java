package com.darcye.clear.reader.entity;

import java.util.LinkedHashSet;
import java.util.Set;

public class BaseEntity {

	private final Set<String> mDrawableIdSet = new LinkedHashSet<String>();

	public Set<String> getDrawableIdSet() {
		return mDrawableIdSet;
	}

	public void setDrawableIdSet(Set<String> drawableIdSet) {
		this.mDrawableIdSet.addAll(drawableIdSet);
	}

	public void addDrawableId(String drawableId) {
		this.mDrawableIdSet.add(drawableId);
	}
	
	private final Set<String> mColorIdSet = new LinkedHashSet<String>();
	
	public Set<String> getColorIdSet() {
		return mColorIdSet;
	}

	public void setColorIdSet(Set<String> colorIdSet) {
		this.mColorIdSet.addAll(colorIdSet);
	}
	
	public void addColorId(String colorId){
		this.mColorIdSet.add(colorId);
	}
	
	private final Set<String> mStringIdSet = new LinkedHashSet<String>();

	public Set<String> getStringIdSet() {
		return mStringIdSet;
	}

	public void setStringIdSet(Set<String> stringIdSet) {
		this.mStringIdSet.addAll(stringIdSet);
	}
	
	public void addStringId(String stringId){
		this.mStringIdSet.add(stringId);
	}
	
	private final Set<String> mIntegerIdSet = new LinkedHashSet<String>();

	public Set<String> getIntegerIdSet() {
		return mIntegerIdSet;
	}

	public void setIntegerIdSet(Set<String> integerIdSet) {
		this.mIntegerIdSet.addAll(integerIdSet);
	}
	
	public void addIntegerId(String integerId){
		this.mIntegerIdSet.add(integerId);
	}
	
	private final Set<String> mBoolIdSet = new LinkedHashSet<String>();

	public Set<String> getBoolIdSet() {
		return mBoolIdSet;
	}

	public void setBoolIdSet(Set<String> boolIdSet) {
		this.mBoolIdSet.addAll(boolIdSet);
	}
	
	public void addBoolId(String boolId){
		this.mBoolIdSet.add(boolId);
	}
	
	private final Set<String> mDimenIdSet = new LinkedHashSet<String>();

	public Set<String> getDimenIdSet() {
		return mDimenIdSet;
	}
	
	public void setDimenIdSet(Set<String> dimenIdSet){
		this.mDimenIdSet.addAll(dimenIdSet);
	}
	
	public void addDimenId(String dimenId){
		this.mDimenIdSet.add(dimenId);
	}
}
