package com.darcye.clear.reader.entity;

import java.util.LinkedHashSet;
import java.util.Set;

public class AnimEntity extends BaseEntity {

	private final Set<String> mAnimIdSet = new LinkedHashSet<String>();

	public Set<String> getAnimIdSet() {
		return mAnimIdSet;
	}

	public void setAnimIdSet(Set<String> animIdSet) {
		this.mAnimIdSet.addAll(animIdSet);
	}

	public void addAnimId(String animId) {
		this.mAnimIdSet.add(animId);
	}
	
	private final Set<String> mAnimatorIdSet = new LinkedHashSet<String>();

	public Set<String> getAnimatorIdSet() {
		return mAnimatorIdSet;
	}

	public void setAnimatorIdSet(Set<String> animatorIdSet) {
		this.mAnimatorIdSet.addAll(animatorIdSet);
	}
	
	public void addAnimatorId(String animatorId){
		this.mAnimatorIdSet.add(animatorId);
	}

	private final Set<String> mTransitionIdSet = new LinkedHashSet<String>();

	public Set<String> getTransitionIdSet() {
		return mTransitionIdSet;
	}

	public void setTransitionIdSet(Set<String> transitionIdSet) {
		this.mTransitionIdSet.addAll(transitionIdSet);
	}
	
	public void addTransitionId(String transitionId){
		this.mTransitionIdSet.add(transitionId);
	}

}
