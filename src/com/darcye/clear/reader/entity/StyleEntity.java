package com.darcye.clear.reader.entity;

import java.util.LinkedHashSet;
import java.util.Set;

public class StyleEntity extends BaseEntity{
	
	private String mStyleName;
	
	private String mParentStyleId;
	
	public String getStyleName() {
		return mStyleName;
	}

	public void setStyleName(String styleName) {
		this.mStyleName = styleName;
	}
	
	public String getParentStyleId() {
		return mParentStyleId;
	}

	public void setParentStyleId(String mParentStyleId) {
		this.mParentStyleId = mParentStyleId;
	}
	
	private final Set<String> mChildrenStyleIds = new LinkedHashSet<String>();

	public Set<String> getChildrenStyleIds() {
		return mChildrenStyleIds;
	}

	public void setChildrenStyleIds(Set<String> mChildrenStyleIds) {
		this.mChildrenStyleIds.addAll(mChildrenStyleIds);
	}

	public void addChildStyleId(String cStyleId){
		this.mChildrenStyleIds.add(cStyleId);
	}
	
	private final Set<String> mAnimIdSet = new LinkedHashSet<String>();

	public Set<String> getAnimIdSet() {
		return mAnimIdSet;
	}

	public void setAnimIdSet(Set<String> animIdSet) {
		this.mAnimIdSet.addAll(animIdSet);
	}
	
	public void addAnimId(String animId){
		this.mAnimIdSet.add(animId);
	}
	
	private final Set<String> mAnimatorIdSet = new LinkedHashSet<String>();

	public Set<String> getAnimatorIdSet() {
		return mAnimatorIdSet;
	}

	public void setAnimatorIdSet(Set<String> animatorIdSet) {
		this.mAnimatorIdSet.addAll( animatorIdSet);
	}
	
	public void addAnimatorId(String animatorId){
		this.mAnimatorIdSet.add(animatorId);
	}

	private final Set<String> mTransitionIdSet = new LinkedHashSet<String>();

	public Set<String> getTransitionIdSet() {
		return mTransitionIdSet;
	}

	public void setTransitionIdSet(Set<String> transitionIdSet) {
		this.mTransitionIdSet.addAll(transitionIdSet);
	}
	
	public void addTransitionId(String transitionId){
		this.mTransitionIdSet.add(transitionId);
	}

	private final Set<String> mIdIdSet = new LinkedHashSet<String>();

	public Set<String> getIdIdSet() {
		return mIdIdSet;
	}
	
	public void setIdIdSet(Set<String> idIdSet){
		this.mIdIdSet.addAll(idIdSet);
	}
	
	public void addIdId(String idId){
		this.mIdIdSet.add(idId);
	}
}
