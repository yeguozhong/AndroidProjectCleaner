package com.darcye.clear.reader.entity;

import java.util.LinkedHashSet;
import java.util.Set;

public class JavaEntity extends BaseEntity{
	
	public String mJavaFile;
	
	public String getJavaFile() {
		return mJavaFile;
	}

	public void setJavaFile(String javaFile) {
		this.mJavaFile = javaFile;
	}

	private final Set<String> mLayoutIdSet = new LinkedHashSet<String>();

	public Set<String> getLayoutIdSet() {
		return mLayoutIdSet;
	}

	public void setLayoutIdSet(Set<String> layoutIdSet) {
		this.mLayoutIdSet.addAll(layoutIdSet);
	}

	public void addLayoutId(String layoutId){
		this.mLayoutIdSet.add(layoutId);
	}
	
	private final Set<String> mStyleIdSet = new LinkedHashSet<String>();

	public Set<String> getStyleIdSet() {
		return mStyleIdSet;
	}

	public void setStyleIdSet(Set<String> styleIdSet) {
		this.mStyleIdSet.addAll(styleIdSet);
	}

	public void addStyleId(String styleId){
		this.mStyleIdSet.add(styleId);
	}
	
	private final Set<String> mMenuIdSet = new LinkedHashSet<String>();

	public Set<String> getMenuIdSet() {
		return mMenuIdSet;
	}

	public void setMenuIdSet(Set<String> menuIdSet) {
		this.mMenuIdSet.addAll(menuIdSet);
	}

	public void addMenuId(String menuId){
		this.mMenuIdSet.add(menuId);
	}

	private final Set<String> mAnimIdSet = new LinkedHashSet<String>();
	
	public Set<String> getAnimIdSet() {
		return mAnimIdSet;
	}

	public void setAnimIdSet(Set<String> animIdSet) {
		this.mAnimIdSet.addAll(animIdSet);
	}
	
	public void addAnimId(String animId){
		this.mAnimIdSet.add(animId);
	}
	
	private final Set<String> mAnimatorIdSet = new LinkedHashSet<String>();

	public Set<String> getAnimatorIdSet() {
		return mAnimatorIdSet;
	}

	public void setAnimatorIdSet(Set<String> animatorIdSet) {
		this.mAnimatorIdSet.addAll(animatorIdSet);
	}
	
	public void addAnimatorId(String animatorId){
		this.mAnimatorIdSet.add(animatorId);
	}

	private final Set<String> mTransitionIdSet = new LinkedHashSet<String>();

	public Set<String> getTransitionIdSet() {
		return mTransitionIdSet;
	}

	public void setTransitionIdSet(Set<String> transitionIdSet) {
		this.mTransitionIdSet.addAll(transitionIdSet);
	}
	
	public void addTransitionId(String transitionId){
		this.mTransitionIdSet.add(transitionId);
	}
	
	private final Set<String> mArrayIdSet = new LinkedHashSet<String>();

	public Set<String> getArrayIdSet() {
		return mArrayIdSet;
	}

	public void setArrayIdSet(Set<String> arrayIdSet) {
		this.mArrayIdSet.addAll(arrayIdSet);
	}
	
	public void addArrayId(String arrayId){
		this.mArrayIdSet.add(arrayId);
	}
	
	private final Set<String> mAttrIdSet = new LinkedHashSet<String>();
	
	public Set<String> getAttrIdSet() {
		return mAttrIdSet;
	}

	public void setAttrIdSet(Set<String> attrIdSet) {
		this.mAttrIdSet.addAll(attrIdSet);
	}
	
	public void addAttrId(String attrId){
		this.mAttrIdSet.add(attrId);
	}
	
	private final Set<String> mStyleableIdSet = new LinkedHashSet<String>();
	
	public Set<String> getStyleableIdSet() {
		return mStyleableIdSet;
	}

	public void setStyleableIdSet(Set<String> styleableIdSet) {
		this.mStyleableIdSet.addAll(styleableIdSet);
	}
	
	public void addStyleableId(String styleableId){
		this.mStyleableIdSet.add(styleableId);
	}
	
	private final Set<String> mIdIdSet = new LinkedHashSet<String>();

	public Set<String> getIdIdSet() {
		return mIdIdSet;
	}
	
	public void setIdIdSet(Set<String> idIdSet){
		this.mIdIdSet.addAll(idIdSet);
	}
	
	public void addIdId(String idId){
		this.mIdIdSet.add(idId);
	}
}
