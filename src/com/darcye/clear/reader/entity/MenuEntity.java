package com.darcye.clear.reader.entity;

import java.util.LinkedHashSet;
import java.util.Set;

public class MenuEntity extends BaseEntity{
	private final Set<String> mLayoutIdSet = new LinkedHashSet<String>();

	public Set<String> getLayoutIdSet() {
		return mLayoutIdSet;
	}

	public void setLayoutIdSet(Set<String> layoutIdSet) {
		this.mLayoutIdSet.addAll(layoutIdSet);
	}

	public void addLayoutId(String layoutId){
		this.mLayoutIdSet.add(layoutId);
	}
	
	private final Set<String> mIdIdSet = new LinkedHashSet<String>();

	public Set<String> getIdIdSet() {
		return mIdIdSet;
	}
	
	public void setIdIdSet(Set<String> idIdSet){
		this.mIdIdSet.addAll(idIdSet);
	}
	
	public void addIdId(String idId){
		this.mIdIdSet.add(idId);
	}
}
