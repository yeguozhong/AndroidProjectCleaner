package com.darcye.clear.reader.entity;

import java.util.LinkedHashSet;
import java.util.Set;

public class ManifestEntity extends BaseEntity{
	
	private final Set<String> mStyleIdSet = new LinkedHashSet<String>();

	public Set<String> getStyleIdSet() {
		return mStyleIdSet;
	}

	public void setStyleIdSet(Set<String> styleIdSet) {
		this.mStyleIdSet.addAll(styleIdSet);
	}

	public void addStyleId(String styleId) {
		this.mStyleIdSet.add(styleId);
	}
}
