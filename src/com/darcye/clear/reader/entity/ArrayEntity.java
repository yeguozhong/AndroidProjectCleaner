package com.darcye.clear.reader.entity;

import java.util.LinkedHashMap;
import java.util.Map;

public class ArrayEntity{
	
	private String mArrayName;
	
	public String getArrayName() {
		return mArrayName;
	}

	public void setArrayName(String arrayName) {
		this.mArrayName = arrayName;
	}

	private final Map<String,Integer> mArrayIdSet = new LinkedHashMap<String,Integer>();

	public void addArrayId(String arrayId,Integer idType){
		this.mArrayIdSet.put(arrayId, idType);
	}

	public static final int STRING_ID = 0x1;
	public static final int INTEGER_ID = 0x2;
	public static final int COLOR_ID = 0x3;
	public static final int DRAWABLE_ID = 0x4;
	public static final int DIMEN_ID = 0x5;
	public static final int BOOL_ID = 0x6;
}
