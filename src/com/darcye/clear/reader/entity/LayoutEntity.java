package com.darcye.clear.reader.entity;

import java.util.LinkedHashSet;
import java.util.Set;

public class LayoutEntity extends BaseEntity {
	
	private String mLayoutFile;
	
	public String getLayoutFile() {
		return mLayoutFile;
	}

	public void setLayoutFile(String layoutFile) {
		this.mLayoutFile = layoutFile;
	}

	private final Set<String> mLayoutIdSet = new LinkedHashSet<String>();

	public Set<String> getLayoutIdSet() {
		return mLayoutIdSet;
	}

	public void setLayoutIdSet(Set<String> layoutIdSet) {
		this.mLayoutIdSet.addAll(layoutIdSet);
	}

	public void addLayoutId(String layoutId){
		this.mLayoutIdSet.add(layoutId);
	}
	
	private final Set<String> mStyleIdSet = new LinkedHashSet<String>();

	public Set<String> getStyleIdSet() {
		return mStyleIdSet;
	}

	public void setStyleIdSet(Set<String> styleIdSet) {
		this.mStyleIdSet.addAll(styleIdSet);
	}

	public void addStyleId(String styleId){
		this.mStyleIdSet.add(styleId);
	}
	
	private final Set<String> mMenuIdSet = new LinkedHashSet<String>();

	public Set<String> getMenuIdSet() {
		return mMenuIdSet;
	}

	public void setMenuIdSet(Set<String> menuIdSet) {
		this.mMenuIdSet.addAll(menuIdSet);
	}

	public void addMenuId(String menuId){
		this.mMenuIdSet.add(menuId);
	}

	private final Set<String> mAnimIdSet = new LinkedHashSet<String>();
	
	public Set<String> getAnimIdSet() {
		return mAnimIdSet;
	}

	public void setAnimIdSet(Set<String> animIdSet) {
		this.mAnimIdSet.addAll(animIdSet);
	}
	
	public void addAnimId(String animId){
		this.mAnimIdSet.add(animId);
	}
	
	private final Set<String> mAnimatorIdSet = new LinkedHashSet<String>();

	public Set<String> getAnimatorIdSet() {
		return mAnimatorIdSet;
	}

	public void setAnimatorIdSet(Set<String> animatorIdSet) {
		this.mAnimatorIdSet.addAll(animatorIdSet);
	}
	
	public void addAnimatorId(String animatorId){
		this.mAnimatorIdSet.add(animatorId);
	}

	private final Set<String> mTransitionIdSet = new LinkedHashSet<String>();

	public Set<String> getTransitionIdSet() {
		return mTransitionIdSet;
	}

	public void setTransitionIdSet(Set<String> transitionIdSet) {
		this.mTransitionIdSet.addAll(transitionIdSet);
	}
	
	public void addTransitionId(String transitionId){
		this.mTransitionIdSet.add(transitionId);
	}

	private final Set<String> mIdIdSet = new LinkedHashSet<String>();

	public Set<String> getIdIdSet() {
		return mIdIdSet;
	}
	
	public void setIdIdSet(Set<String> idIdSet){
		this.mIdIdSet.addAll(idIdSet);
	}
	
	public void addIdId(String idId){
		this.mIdIdSet.add(idId);
	}
}
