package com.darcye.clear.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.darcye.clear.reader.entity.BaseEntity;
import com.darcye.clear.utils.ResRegex;
import com.darcye.clear.utils.ResUtil;

/**
 * 扫描文件
 * @author Darcy
 * @date 2014-9-26 下午2:52:26
 * @version V1.0
 */
public abstract class BaseFileReader {
	
	private File mFile;
	
	private OnFileReadListener mOnFileReadListener;
	
	private BaseEntity mEntity;
	
	public BaseFileReader(File file){
		this.mFile = file;
	}
	
	void setEntity(BaseEntity entity){
		this.mEntity = entity;
	}
	
	public BaseFileReader setOnFileReadListener(OnFileReadListener onFileReadListener) {
		this.mOnFileReadListener = onFileReadListener;
		return this;
	}

	public void scan() throws FileNotFoundException{
		if(mFile == null || !mFile.exists() || !mFile.isFile())
			throw new RuntimeException("file is not available...");
		if(onBeforeRead(mFile)){
			onFinished();
			return;
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(mFile)));
		String lineText;
		try {
			while((lineText = reader.readLine())!=null){
				if(!lineText.trim().isEmpty())
					onLineRead(lineText);
			}
			onFinished();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(mOnFileReadListener!=null)
			mOnFileReadListener.onReaded(mEntity);
	}
	
	 String getIdName(String resId){
		return ResUtil.getIdName(resId);
	}
	
	/**
	 * 如果返回true，则不再进行逐行扫描
	 * @param file
	 * @return
	 */
	protected boolean onBeforeRead(File file){
		return false;
	};
	
	/**
	 * 每读一行记录返回
	 * @param lineText
	 */
	protected abstract void onLineRead(String lineText);
	
	/**
	 * 读完文件返回
	 */
	protected void onFinished(){}
	
	enum ResType{
		LAYOUT,STYLE,MENU,DRAWABLE,ANIM,ANIMATOR,TRANSITION,COLOR,STRING,ARRAY,INTEGER,BOOL,DIMEN,ATTR,STYLEABLE,ID
	}
	
	interface OnObtainResId{
		void onObtain(ResType type, String resId);
	}
	
	void obtainResIdOnRegex(Pattern p,String content,OnObtainResId onObtainResId){
		String resId;
		String resIdName;
		Matcher m = p.matcher(content);
		while(m.find()){
			resId = m.group();
			resIdName= getIdName(resId);
			if(resId.matches(ResRegex.JAVA_LAYOUT_REGEX)||resId.matches(ResRegex.XML_LAYOUT_REGEX)){
				onObtainResId.onObtain(ResType.LAYOUT, resIdName);
			}else if(resId.matches(ResRegex.JAVA_STYLE_REGEX)||resId.matches(ResRegex.XML_STYLE_REGEX)){
				onObtainResId.onObtain(ResType.STYLE, resIdName);
			}else if(resId.matches(ResRegex.JAVA_DRAWABLE_REGEX)||resId.matches(ResRegex.XML_DRAWABLE_REGEX)){
				onObtainResId.onObtain(ResType.DRAWABLE, resIdName);
			}else if(resId.matches(ResRegex.JAVA_ANIM_REGEX)||resId.matches(ResRegex.XML_ANIM_REGEX)){
				onObtainResId.onObtain(ResType.ANIM, resIdName);
			}else if(resId.matches(ResRegex.JAVA_ANIMATOR_REGEX)||resId.matches(ResRegex.XML_ANIMATOR_REGEX)){
				onObtainResId.onObtain(ResType.ANIMATOR, resIdName);
			}else if(resId.matches(ResRegex.JAVA_TRANSITION_REGEX)||resId.matches(ResRegex.XML_TRANSITION_REGEX)){
				onObtainResId.onObtain(ResType.TRANSITION, resIdName);
			}else if(resId.matches(ResRegex.JAVA_MENU_REGEX)||resId.matches(ResRegex.XML_MENU_REGEX)){
				onObtainResId.onObtain(ResType.MENU, resIdName);
			}else if(resId.matches(ResRegex.JAVA_COLOR_REGEX)||resId.matches(ResRegex.XML_COLOR_REGEX)){
				onObtainResId.onObtain(ResType.COLOR, resIdName);
			}else if(resId.matches(ResRegex.JAVA_STRING_REGEX)||resId.matches(ResRegex.XML_STRING_REGEX)){
				onObtainResId.onObtain(ResType.STRING, resIdName);
			}else if(resId.matches(ResRegex.JAVA_INTEGER_REGEX)||resId.matches(ResRegex.XML_INTEGER_REGEX)){
				onObtainResId.onObtain(ResType.INTEGER, resIdName);
			}else if(resId.matches(ResRegex.JAVA_BOOL_REGEX)||resId.matches(ResRegex.XML_BOOL_REGEX)){
				onObtainResId.onObtain(ResType.BOOL, resIdName);
			}else if(resId.matches(ResRegex.JAVA_DIMEN_REGEX)||resId.matches(ResRegex.XML_DIMEN_REGEX)){
				onObtainResId.onObtain(ResType.DIMEN, resIdName);
			}else if(resId.matches(ResRegex.JAVA_ARRAY_REGEX)){
				onObtainResId.onObtain(ResType.ARRAY, resIdName);
			}else if(resId.matches(ResRegex.JAVA_ATTR_REGEX)){
				onObtainResId.onObtain(ResType.ATTR, resIdName);
			}else if(resId.matches(ResRegex.JAVA_STYLEABLE_REGEX)){
				onObtainResId.onObtain(ResType.STYLEABLE, resIdName);
			}else if(resId.matches(ResRegex.JAVA_ID_REGEX)||resId.matches(ResRegex.XML_ID_REGEX)){
				onObtainResId.onObtain(ResType.ID , resIdName);
			}
		}
	}
}
