package com.darcye.clear.reader;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import com.darcye.clear.reader.entity.StyleEntity;
import com.darcye.clear.utils.ResRegex;

/**
 * 读取Style文件
 * @author Darcy
 * @date 2014-9-28 下午8:00:48
 * @version V1.0
 */
public class StyleFileReader extends BaseFileReader {
	
	public interface OnStyleFileReaderListener{
		void onComplete(Map<String,StyleEntity> styleEntityMap);
	}
	
	private OnStyleFileReaderListener mOnStyleFileReaderListener;
	
	private Map<String,StyleEntity> mStyleEntityMap;
	
	private Document mDocument;
	
	public StyleFileReader(File file) {
		super(file);
	}

	public StyleFileReader setOnStyleFileReaderListener(OnStyleFileReaderListener l) {
		this.mOnStyleFileReaderListener = l;
		return this;
	}

	@Override
	protected boolean onBeforeRead(File file) {
		String elName;
		String parentStyleId;
		StyleEntity styleEntity ;
		Attribute nameAttr,parentAttr;
		List<Element> childrenEls;
		mStyleEntityMap = new LinkedHashMap<String, StyleEntity>();
		try {
			SAXBuilder builder = new SAXBuilder();
			mDocument = builder.build(file);
			Element rootEl = mDocument.getRootElement();
			List<Element> styleEls = rootEl.getChildren();
			String styleName;
			for (Element styleEl : styleEls) {
				 elName = styleEl.getName();
				 styleEntity = new StyleEntity();
				 if(elName.equals("style")){
					 nameAttr = styleEl.getAttribute("name");
					 parentAttr = styleEl.getAttribute("parent");
					 styleName = nameAttr.getValue();
					 styleEntity.setStyleName(styleName);
					 if(parentAttr!=null){
						 parentStyleId = getParentStyleId(parentAttr.getValue());
						 styleEntity.setParentStyleId(parentStyleId);
					 }
					 childrenEls = styleEl.getChildren();
					 String childElValue;
					 for (Element childEl : childrenEls) {
						 childElValue = childEl.getValue();
						 if(childElValue.matches(ResRegex.XML_STYLE_REGEX)){
							 styleEntity.addChildStyleId(getIdName(childElValue));
						 }else if(childElValue.matches(ResRegex.XML_DRAWABLE_REGEX)){
							 styleEntity.addDrawableId(getIdName(childElValue));
						 }else if(childElValue.matches(ResRegex.XML_ANIM_REGEX)){
							 styleEntity.addAnimId(getIdName(childElValue));
						 }else if(childElValue.matches(ResRegex.XML_ANIMATOR_REGEX)){
							 styleEntity.addAnimatorId(getIdName(childElValue));
						 }else if(childElValue.matches(ResRegex.XML_TRANSITION_REGEX)){
							 styleEntity.addTransitionId(getIdName(childElValue));
						 }else if(childElValue.matches(ResRegex.XML_COLOR_REGEX)){
							 styleEntity.addColorId(getIdName(childElValue));
						 } else if(childElValue.matches(ResRegex.XML_STRING_REGEX)){
							 styleEntity.addStringId(getIdName(childElValue));
						 } else if(childElValue.matches(ResRegex.XML_INTEGER_REGEX)){
							 styleEntity.addIntegerId(getIdName(childElValue));
						 } else if(childElValue.matches(ResRegex.XML_BOOL_REGEX)){
							 styleEntity.addBoolId(getIdName(childElValue));
						 } else if(childElValue.matches(ResRegex.XML_DIMEN_REGEX)){
							 styleEntity.addDimenId(getIdName(childElValue));
						 } else if(childElValue.matches(ResRegex.XML_ID_REGEX)){
							 styleEntity.addIdId(getIdName(childElValue));
						 }
					 }
					 mStyleEntityMap.put(styleName, styleEntity);
				 }
			}
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}

	public Document getDocument() {
		return mDocument;
	}

	private String getParentStyleId(String resId){
		String idName = null;
		if(resId.matches(ResRegex.XML_STYLE_REGEX)){
			idName = resId.substring(resId.lastIndexOf("/")+1);
		}else if(resId.matches("\\w+")){
			idName = resId;
		}
		return idName;
	}
	
	@Override
	protected void onFinished() {
		if(mOnStyleFileReaderListener!=null)
			mOnStyleFileReaderListener.onComplete(mStyleEntityMap);
	}

	@Override
	protected void onLineRead(String lineText) {
	}
}
