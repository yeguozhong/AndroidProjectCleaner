package com.darcye.clear.reader;

import java.io.File;
import java.util.regex.Pattern;

import com.darcye.clear.reader.entity.AnimEntity;
import com.darcye.clear.utils.ResRegex;

public class AnimFileReader extends BaseFileReader {

	/**
	 * 匹配布局文件的正则
	 */
	private static final Pattern resPattern = Pattern.compile(ResRegex.ANIM_RES_ID_REGEX);
	
	private AnimEntity mAnimEntity = new AnimEntity();
	
	//压缩成单行存储
	private StringBuilder mXmlContent = new StringBuilder(); 

	public AnimFileReader(String fileName){
		this(new File(fileName));
	}
	
	public AnimFileReader(File file) {
		super(file);
	}

	@Override
	protected void onLineRead(String lineText) {
		mXmlContent.append(lineText);
	}

	@Override
	protected void onFinished() {
		obtainResIds();
		setEntity(mAnimEntity);
	}
	
	/**
	 * 获取资源Id
	 */
	void obtainResIds(){
		obtainResIdOnRegex(resPattern, mXmlContent.toString(), new OnObtainResId() {
			public void onObtain(ResType type, String resIdName) {
				switch (type) {
				case DRAWABLE:
					mAnimEntity.addDrawableId(resIdName);
					break;
				case ANIM:
					mAnimEntity.addAnimId(resIdName);
					break;
				case ANIMATOR:
					mAnimEntity.addAnimatorId(resIdName);
					break;
				case TRANSITION:
					mAnimEntity.addTransitionId(resIdName);
					break;
				case COLOR:
					mAnimEntity.addColorId(resIdName);
					break;
				case STRING:
					mAnimEntity.addStringId(resIdName);
					break;
				case INTEGER:
					mAnimEntity.addIntegerId(resIdName);
					break;
				case BOOL:
					mAnimEntity.addBoolId(resIdName);
					break;
				case DIMEN:
					mAnimEntity.addDimenId(resIdName);
					break;
				default:
					break;
				}
			}
		});
	}
}
