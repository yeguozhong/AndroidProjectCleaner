package com.darcye.clear.reader;

import java.io.File;
import java.util.regex.Pattern;

import com.darcye.clear.reader.entity.DrawableEntity;
import com.darcye.clear.utils.ResRegex;

public class DrawableFileReader extends BaseFileReader {

	/**
	 * 匹配布局文件的正则
	 */
	private static final Pattern resPattern = Pattern.compile(ResRegex.DRAWABLE_RES_ID_REGEX);

	//压缩成单行存储
	private StringBuilder mXmlContent = new StringBuilder(); 

	private DrawableEntity mDrawableEntity = new DrawableEntity();
	
	public DrawableFileReader(File file) {
		super(file);
	}

	@Override
	protected void onLineRead(String lineText) {
		mXmlContent.append(lineText);
	}

	@Override
	protected void onFinished() {
		obtainResIds();
		setEntity(mDrawableEntity);
	}
	
	/**
	 * 获取资源Id
	 */
	void obtainResIds(){
		obtainResIdOnRegex(resPattern, mXmlContent.toString(), new OnObtainResId() {
			public void onObtain(ResType type, String resIdName) {
				switch (type) {
				case DRAWABLE:
					mDrawableEntity.addDrawableId(resIdName);
					break;
				case COLOR:
					mDrawableEntity.addColorId(resIdName);
					break;
				case INTEGER:
					mDrawableEntity.addIntegerId(resIdName);
					break;
				case BOOL:
					mDrawableEntity.addBoolId(resIdName);
					break;
				case DIMEN:
					mDrawableEntity.addDimenId(resIdName);
					break;
				default:
					break;
				}
			}
		});
	}
}
