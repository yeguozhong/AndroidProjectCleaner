package com.darcye.clear.utils;

public class ResUtil {
	
	/**
	 * 截取resId的名称
	 * @param resId
	 * @return
	 */
	public static String getIdName(String resId){
		String idName = null;
		if(resId.startsWith("@")){
			idName = resId.substring(resId.lastIndexOf("/")+1);
		}
		if(resId.startsWith("R")){
			return resId.substring(resId.lastIndexOf(".")+1);
		}
		return idName;
	}
}
