package com.darcye.clear.utils;


public class ResRegex {
	
	public static final String LAYOUT_RES_ID_REGEX = "@((layout)|(style)|(drawable)|(anim)|(animator)|(transition)|(color)|(string)|(integer)|(bool)|(dimen)|(id))/[\\w\\.]+";
	
	public static final String MANIFEST_RES_ID_REGEX = "@((style)|(drawable)|(string)|(integer)|(bool)|(id))/[\\w\\.]+";
	
	public static final String MENU_RES_ID_REGEX = "@((drawable)|(string)|(integer)|(bool)|(dimen)|(id))/[\\w\\.]+";
	
	public static final String ANIM_RES_ID_REGEX = "@((drawable)|(anim)|(animator)|(transition)|(color)|(string)|(integer)|(bool)|(dimen))/[\\w\\.]+";
	
	public static final String DRAWABLE_RES_ID_REGEX = "@((drawable)|(color)|(integer)|(bool)|(dimen))/[\\w\\.]+";
	
	public static final String JAVA_RES_ID_REGEX = "R\\.((layout)|(style)|(menu)|(anim)|(animator)|(transition)|(drawable)|(array)|(color)|(string)|(integer)|(bool)|(dimen)|(attr)|(styleable)|(id))\\.\\w+";

	public static final String XML_LAYOUT_REGEX = "@layout/[\\w\\.]+";
	public static final String JAVA_LAYOUT_REGEX = "R\\.layout\\.\\w+";
	
	public static final String XML_STYLE_REGEX = "@style/[\\w\\.]+";
	public static final String JAVA_STYLE_REGEX = "R\\.style\\.\\w+";
	
	public static final String XML_DRAWABLE_REGEX = "@drawable/[\\w\\.]+";
	public static final String JAVA_DRAWABLE_REGEX = "R\\.drawable\\.\\w+";
	
	public static final String XML_MENU_REGEX = "@menu/[\\w\\.]+";
	public static final String JAVA_MENU_REGEX = "R\\.menu\\.\\w+";

	public static final String XML_ANIM_REGEX = "@anim/[\\w\\.]+";
	public static final String JAVA_ANIM_REGEX = "R\\.anim\\.\\w+";
	
	public static final String XML_ANIMATOR_REGEX = "@animator/[\\w\\.]+";
	public static final String JAVA_ANIMATOR_REGEX = "R\\.animator\\.\\w+";
	
	public static final String XML_TRANSITION_REGEX = "@transition/[\\w\\.]+";
	public static final String JAVA_TRANSITION_REGEX = "R\\.transition\\.\\w+";
	
	public static final String XML_COLOR_REGEX = "@color/[\\w\\.]+";
	public static final String JAVA_COLOR_REGEX = "R\\.color\\.\\w+";
	
	public static final String XML_STRING_REGEX = "@string/[\\w\\.]+";
	public static final String JAVA_STRING_REGEX = "R\\.string\\.\\w+";

	public static final String JAVA_ARRAY_REGEX = "R\\.array\\.\\w+";

	public static final String XML_INTEGER_REGEX = "@integer/[\\w\\.]+";
	public static final String JAVA_INTEGER_REGEX = "R\\.integer\\.\\w+";
	
	public static final String XML_BOOL_REGEX = "@bool/[\\w\\.]+";
	public static final String JAVA_BOOL_REGEX = "R\\.bool\\.\\w+";
	
	public static final String XML_DIMEN_REGEX = "@dimen/[\\w\\.]+";
	public static final String JAVA_DIMEN_REGEX = "R\\.dimen\\.\\w+";
	
	public static final String JAVA_ATTR_REGEX = "R\\.attr\\.\\w+";
	
	public static final String JAVA_STYLEABLE_REGEX = "R\\.styleable\\.\\w+";
	
	public static final String XML_ID_REGEX = "@id/[\\w\\.]+";
	public static final String JAVA_ID_REGEX = "R\\.id\\.\\w+";
	
}
