**note: 该项目已过时**

软件名称: 
Android Project Cleaner (Android工程清理器)

软件功能：
在Android应用开发过程中，往往会由于版本更迭或者其他的原因产生了很多多余的资源，比如layout, drawable,string等许多资源，如果要求程序员在开发的过程中就留意清理工作，无疑会带来很多没必要的工作量，而后期清理的难度又比较大。因此，该软件就是为了在发布前做的最后一次清理工作，把没用到的资源删除掉，这样，程序员就可以在开发时专注于开发工作，而且对于最后发布的包也会变得更小。

已完成功能：
目前已经可以简单地清理以下资源类型(R.[resource type] or @[resource type]):
*动画(anim | animator | transition)
*数组(array)
*属性(attr)
*基本类型(bool | integer | string)
*颜色(color)
*图片(drawable)
*布局(layout)
*菜单(menu)
*styleable
*style
*其他类型(id | dimen)